# Perl5 Unit Tests

Path-based, metadata-driven unit tests for Perl5 prove.

For a description of the logic behind these tests see also:

<https://www.slideshare.net/lembark/path-toknowlege>
